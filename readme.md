# BA-STRAPI

Misitioba.com strapi instance

## Local Development

- Create a folder and cd inside.

(docker rm -f strapi || true); \
docker run -d \
  --name strapi \
  -e NODE_ENV=production \
  -e DATABASE_CLIENT=mongo \
  -e DATABASE_NAME=strapi \
  -e DATABASE_HOST=host \
  -e DATABASE_PORT=27017 \
  -e DATABASE_USERNAME=xxxx \
  -e DATABASE_PASSWORD=xxxx \
  -v $(pwd)/app:/srv/app \
  -p 1337:1337 \
  strapi/strapi

## Deployment

```sh
#1337 (strapi:1337 for caddy)
cd /root/git/ba-strapi; \
(docker rm -f strapi || true); \
docker run -d \
  --name strapi \
  -e NODE_ENV=production \
  -e DATABASE_CLIENT=mongo \
  -e DATABASE_NAME=strapi \
  -e DATABASE_HOST=host \
  -e DATABASE_PORT=27017 \
  -e DATABASE_USERNAME=xxxx \
  -e DATABASE_PASSWORD=xxxx \
  -v $(pwd)/app:/srv/app \
  --net=caddy-node_caddy --net-alias=strapi \
  strapi/strapi strapi start
````

